<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\TripSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Trips';
$this->params['breadcrumbs'][] = $this->title;

print_r($dataProvider->query->createCommand()->getRawSql());

//print_r($dataProvider->getCount());
?>
<div class="trip-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php  echo $this->render('_search', ['model' => $searchModel]); ?>

    

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'corporate_id',
            'number',
            'user_id',
            //'tsnum',
            /*[
                'label' => 'Airport Name',
                'attribute' => 'airportName',
                'value' => function($model) {
                    return join('; ', yii\helpers\ArrayHelper::map($model->airportNames, 'id', 'value'));                   
                    
                },

            ],*/
            'created_at',
            
            //'updated_at',
            //'coordination_at',
            //'saved_at',
            //'tag_le_id',
            //'trip_purpose_id',
            //'trip_purpose_parent_id',
            //'trip_purpose_desc:ntext',
            'status',

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
