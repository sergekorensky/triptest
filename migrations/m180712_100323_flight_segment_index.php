<?php

use yii\db\Migration;

/**
 * Class m180712_100323_flight_segment_index
 */
class m180712_100323_flight_segment_index extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex('depAirportId_flight_id','flight_segment','depAirportId,flight_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        //echo "m180711_170243_flight_segment_index cannot be reverted.\n";
        $this->dropIndex('depAirportId_flight_id');
        //return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180712_100323_flight_segment_index cannot be reverted.\n";

        return false;
    }
    */
}
